import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { selectBook } from "../actions/index";

class BookList extends Component {
  renderList() {
    return this.props.books.map(book => (
      <li
        key={book.title}
        onClick={() => {
          this.props.selectBook(book);
        }}
        className="list-group-item"
      >
        {book.title}
      </li>
    ));
  }

  render() {
    return <ul className="list-group col-sm-4">{this.renderList()}</ul>;
  }
}

function mapStateToProps(state) {
  // whatever returned will show as props in bookList
  // whenever application state changes to books, this will rerender
  return {
    books: state.books
  };
}

// anything returned from this function will end up as props on the bookList container
// purpose is to make the select book action creator bound so that it can be dispatched then after computations on state
function mapDispatchToProps(dispatch) {
  // whenever selectbook is called, the result should be passed to all reducers
  return bindActionCreators({ selectBook: selectBook }, dispatch);
}

// promote bookList from a component to a container by CONNECTING the state to the component
export default connect(mapStateToProps, mapDispatchToProps)(BookList);
