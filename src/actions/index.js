// selectBook is an action creator thus needs to return an action - obj with a type property
// type of action is always an object with a type and a payload
export const selectBook = book => ({
  type: "BOOK_SELECTED",
  payload: book
});
