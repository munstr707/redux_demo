import React from "react";

import BookList from "./containers/book-list";
import BookDetail from "./containers/book-detail";

const App = () => (
  <React.Fragment>
    <BookList />
    <BookDetail />
  </React.Fragment>
);

export default App;
