export default function() {
  return [
    { title: "Javascript - The good Parts", pages: 101 },
    { title: "Eloquent Javascript", pages: 300 },
    { title: "You Dont know JS", pages: 500 },
    { title: "The better parts", pages: 101 }
  ];
}
